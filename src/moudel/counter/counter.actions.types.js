export const COUNTER = {
  counterAdd: "COUNTER_ADD",
  counterSub: "COUNTER_SUB",
  counterReset: "COUNTER_RESET",
  counterAddBy5: "COUNTER_ADD_BY_5",
  counterSubBy5: "COUNTER_SUB_BY_5",
};
