import { COUNTER } from "../counter.actions.types.js";

const init = {
  count: 0,
};

export default function counter(state = init, action) {
  switch (action.type) {
    case COUNTER.counterAdd:
      return { count: state.count + 1 };
    case COUNTER.counterSub:
      return { count: state.count - 1 };
    case COUNTER.counterAddBy5:
      return { count: state.count + 5 };
    case COUNTER.counterSubBy5:
      return { count: state.count - 5 };
    case COUNTER.counterReset:
      return { count: 0 };
    default:
      return state;
  }
}
