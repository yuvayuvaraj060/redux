import React from "react";
import { connect } from "react-redux";
import {
  increment,
  decrement,
  incrementBy5,
  decrementBy5,
  reset,
} from "../counter.actions";
const Counter = (props) => {
  console.log(props);
  const { count } = props;
  const { increment, decrement, incrementBy5, decrementBy5, reset } = props;
  // const {} = props.actions
  return (
    <div style={{ margin: "20px" }}>
      Redux-Counter:
      <div
        className="count__value"
        style={{
          fontSize: "56px",
          color: "orangered",
          textAlign: "center",
          margin: "20px",
        }}
      >
        {count}
      </div>
      <div
        className="btn"
        style={{
          display: "flex",
          justifyContent: "space-around",
          margin: "40px 430px",
        }}
      >
        <button onClick={increment}>Increment</button>
        <button onClick={decrement}>Decrement</button>
        <button onClick={incrementBy5}>Increment By 5</button>
        <button onClick={decrementBy5}>Decrement By 5</button>
        <button onClick={reset}>Reset</button>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    count: state.counter.count,
  };
};

export default connect(mapStateToProps, {
  increment,
  decrement,
  incrementBy5,
  decrementBy5,
  reset,
})(Counter);
