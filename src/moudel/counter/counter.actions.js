import { COUNTER } from "./counter.actions.types";
export const increment = () => {
  return (dispatch) => {
    dispatch({ type: COUNTER.counterAdd });
  };
};

export const decrement = () => {
  return (dispatch) => {
    dispatch({ type: COUNTER.counterSub });
  };
};

export const incrementBy5 = () => {
  return (dispatch) => {
    dispatch({ type: COUNTER.counterAddBy5 });
  };
};

export const decrementBy5 = () => {
  return (dispatch) => {
    dispatch({ type: COUNTER.counterSubBy5 });
  };
};

export const reset = () => {
  return (dispatch) => {
    dispatch({ type: COUNTER.counterReset });
  };
};
