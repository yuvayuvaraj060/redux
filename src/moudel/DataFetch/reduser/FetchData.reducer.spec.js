import { FetchDataActions } from "../FetchData.actions.type.js";

const init = {
  id: null,
  first_name: "",
  DOB: null,
};

export default function FetchData(state = init, action) {
  switch (action.type) {
    case FetchDataActions.GET_DATA:
      return action.data;

    default:
      return state;
  }
}
