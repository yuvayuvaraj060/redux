import { FetchDataActions } from "./FetchData.actions.type.js";
import { FetchData } from "./services/index";
export const GET_DATA = () => {
  return async (dispatch) => {
    const result = await FetchData();
    if (result.status === 200) {
      dispatch({
        type: FetchDataActions.GET_DATA,
        data: result.data,
      });
    }
    return result;
  };
};

export const ADD_DATA = (data) => {
  return (dispatch) => {
    dispatch({
      type: FetchDataActions.ADD_DATA,
      data: data,
    });
  };
};

export const REMOVE_DATA = (id) => {
  return (dispatch) => {
    dispatch({
      type: FetchDataActions.REMOVE_DATA,
      data: id,
    });
  };
};
