import axios from "axios";
const BASE_URL = "http://localhost:5000";
const HTTP = axios.create({
  baseURL: BASE_URL,
});
export const FetchData = async () => {
  return await HTTP.get("/user").then((res) => {
    console.log(res, "res");
    return res;
  });
};
