import React, { useEffect } from "react";
import { connect } from "react-redux";
import { ADD_DATA, GET_DATA, REMOVE_DATA } from "../FetchData.actions.js";

export const FetchData = (props) => {
  useEffect(() => {
    props.GET_DATA();
  }, []);
  console.log(props);
  const { FetchData } = props;
  return (
    <div>
      {FetchData.map((item) => {
        return (
          <div className="top">
            {console.log(item, typeof item)}
            <pre>{JSON.stringify(item, null, 2)}</pre>
          </div>
        );
      })}
    </div>
  );
};

const mapStateToProps = (state) => {
  console.log(state);
  return { FetchData: state.FetchData };
};

export default connect(mapStateToProps, { ADD_DATA, GET_DATA, REMOVE_DATA })(
  FetchData
);
