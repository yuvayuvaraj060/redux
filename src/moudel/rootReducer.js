import { combineReducers } from "redux";
import counter from "./counter/reduser/counter.reducer.spec.js";
import FetchData from "./DataFetch/reduser/FetchData.reducer.spec";
export default combineReducers({
  counter,
  FetchData,
});
