import "./App.css";
import Counter from "./moudel/counter/components/Counter.js";
import FetchData from "./moudel/DataFetch/components/FetchData.js";
function App() {
  return (
    <div className="App">
      <Counter />
      <FetchData />
    </div>
  );
}

export default App;
